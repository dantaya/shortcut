# Shortcut
A library to add dynamic keyboard shortcuts to any project with ease

## Usage
You just need to add the script and intialize the library. You can pass shortcuts into ```init()``` or add them after by calling ```add()```.

The key combination is just the key values separated with a '+'.

Then you pass in the function.

### Stand-alone
```javascript
<script src="./shortcut.min.js"></script>

<script>

// Initialize the shortcut tool passing in an object of shortcuts
Shortcut.init({'a+b+c': shortcutFunction});

// Remove all shortcuts and clear the listeners - use this if you want to kill all the shortcuts
Shortcut.clear();

// Add any number of singulare shortcuts after intialization
Shortcut.add('a+c+d', shortcutFunction1);

// Remove a shortcut from the system
Shortcut.remove('a+c+d');

</script>
```
