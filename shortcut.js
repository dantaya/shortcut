// Shortcut
// library to handle keyboard shortcuts and easter eggs
//
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define([], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory();
    } else {
        root.Shortcut = factory();
    }
}(typeof self !== 'undefined' ? self : window, function () {

    // Default node
    const oNode = `{"nodes": {}, "callback": null}`;

    // Key store
    let _store = {};

    // House and handle the shortcuts
    let _shortcuts = {
        tree: JSON.parse(oNode),

        // Add a new shortcut to the system
        add: function (shortcut, _callback) {
            // Break the command into parts
            let parts = shortcut.split('+');

            // Add the shortcut to the tree
            this._add(this.tree.nodes, parts, _callback);
        },

        // Remove a shortcut
        remove: function(shortcut) {
            // Break the command into parts
            let parts = shortcut.split('+');
        
            // Remove the shortcut from the tree
            this.tree.nodes = this._remove(this.tree.nodes, parts);
        },

        // Check to see if the store corresponds to a shortcut
        find: function () {
            // Find the shortcut in the tree
            result = this._find(this.tree.nodes, Object.keys(_store));
        },

        // Recursively add nodes to the tree for the shortcut
        _add: function (nodes, parts, _callback) {
            // Get he current part of the shortcut
            let cPart = parts[0];
            parts.shift();

            // If this is the last part, we just want add the call back
            if (parts.length === 0) {
                // Create a new node
                let nNode = JSON.parse(oNode);
                nNode.callback = _callback;

                // Add the node to the tree
                nodes[cPart] = nNode;
                return;
            }

            // If the node doesn't exist add it
            if (!nodes[cPart]) {
                nodes[cPart] = JSON.parse(oNode);
            }

            // If the node exists go into it
            this._add(nodes[cPart].nodes, parts, _callback);
        },

        // Recursively remove all nodes from the tree that don't have other children
        _remove: function (nodes, keys) {
            // If the node matches the last node, remove the callback
            if (keys.length == 1 && nodes[keys[0]]) {
                nodes[keys[0]].callback = null;
                if (nodes[keys[0]].nodes.length == 0) {
                    return nodes;
                }

                // Remove the node
                delete(nodes[keys[0]])
                return nodes;
            }

            // Loop over the keys to check for matches
            for (let index in keys) {
                // The key doesn't exist in the node list so continue to the next one
                if (!nodes[keys[index]]) {
                    continue;
                }

                // If the node has a matching child go into it
                if (Object.keys(nodes[keys[index]].nodes).length > 0) {
                    nodes[keys[index]].nodes = this._remove({...nodes[keys[index]].nodes}, keys.filter(function (value) { return value == keys[index] ? false : true }));
                }
            }

            return {...nodes};
        },

        // Recursively check the tree to find out if the store matches
        _find: function (nodes, keys) {
            // Fire the call back because there are no more keys and there is a match
            if (keys.length == 1 && nodes[keys[0]]) {
                // Make sure the call back is a function before running it
                if (typeof nodes[keys[0]].callback == 'function') {
                    nodes[keys[0]].callback();
                }
                
                return;
            }

            // Loop over the keys to check for matches
            for (let index in keys) {
                // The key doesn't exist in the node list so continue to the next one
                if (!nodes[keys[index]]) {
                    continue;
                }

                // If there node has a matching child check it
                if (Object.keys(nodes[keys[index]].nodes).length > 0) {
                    this._find({...nodes[keys[index]].nodes}, keys.filter(function (value) { return value == keys[index] ? false : true }));
                }
            }

            return;
        }
    };

    // Boot load and setup initial set of shortcuts
    let init = function (shortcuts = {}) {
        // Loop over shortcuts and add it to the shortcut list
        for (let shortcut in shortcuts) {
            _shortcuts.add(shortcut, shortcuts[shortcut]);
        }

        // Set up the watcher to listen for key presses
        window.addEventListener('keydown', handleDown);
        window.addEventListener('keyup', handleUp);
    };

    // Clear the shortcut listeners
    let clear = function () {
        _shortcuts.tree = JSON.parse(oNode);
        window.removeEventListener('keydown', handleDown);
        window.removeEventListener('keyup', handleUp);
    }

    // Add a single shortcut at a time
    let add = function (shortcut = '', _callback) {
        if (shortcut !== '') {
            _shortcuts.add(shortcut, _callback);
        }
    };

    // Remove a single shortcut at a time
    let remove = function (shortcut = '') {
        if (shortcut !== '') {
            _shortcuts.remove(shortcut);
        }
    }

    // Handle the key press
    let handleDown = function (event) {
        // If the key isn't already in the store add it, and determine if the we need to fire a shortcut
        if (!_store[event.key]) {
            // Add key to store
            _store = { ..._store, [event.key]: true };

            // Check the tree
            _shortcuts.find();
            return;
        }
    }

    // Handle the key release
    let handleUp = function (e) {
        const { [event.key]: id, ...rest } = _store;
        _store = rest;
    }

    // Return the main object to use
    return {
        init: init,
        clear: clear,
        add: add,
        remove: remove
    };
}));